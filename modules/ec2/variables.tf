variable "instance_ami" {
    default = "ami-05b891753d41ff88f"
}
variable "instance_type" {
    default = "t3.micro"
}

variable "key_name" {
    default = "srumy-key"
}
variable "vpc_id" {}
variable "subnet1_id" {}
variable "subnet2_id" {}
variable "subnet3_id" {}
variable "subnet4_id" {}

variable "security_groups" {}
variable "target_arn" {}
variable "target_arn_wp" {}
variable "min_size" {
	default = "1"
}
variable "desired_capacity" {
	default	="2"
}
variable "max_size" {
	default	= "4"
}
