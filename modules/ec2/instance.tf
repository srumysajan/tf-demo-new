
# Nginx  ec2 instance
resource "aws_instance" "nginx"{
  ami = var.instance_ami
  instance_type = var.instance_type
  key_name = var.key_name
  vpc_security_group_ids = var.security_groups
  subnet_id = var.subnet1_id
  tags = {
      Name = "Nginx-host"
  }
}

##Launch Configuration for Public Instance ##

resource "aws_launch_configuration" "nginx-lc" {
  name_prefix   = "nginx-lc"
  image_id      = aws_ami_from_instance.ami-nginx.id # Amazon Linux 2 AMI (HVM), SSD Volume Type
  instance_type = var.instance_type
  key_name      = var.key_name
  security_groups             = var.security_groups
  associate_public_ip_address = true

  lifecycle {
    create_before_destroy = true
  }

}
## Autoscaling for Public Instance ##

resource "aws_autoscaling_group" "asg-nginx" {
  name = "${aws_launch_configuration.nginx-lc.name}-asg"


  min_size         = var.min_size
  desired_capacity = var.desired_capacity
  max_size         = var.max_size



  launch_configuration = aws_launch_configuration.nginx-lc.name


  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]


  metrics_granularity = "1Minute"


  vpc_zone_identifier = [
    var.subnet1_id,
    var.subnet3_id
  ]


  # Required to redeploy without an outage.
  lifecycle {
    create_before_destroy = true
  }

  tag {

    key                 = "Name"
    value               = "web"
    propagate_at_launch = true
  }
}
#### AMI Creation for Nginx Instance#####

resource "aws_ami_from_instance" "ami-nginx" {
  name               = "ami-nginx"
  source_instance_id = aws_instance.nginx.id
}
###### Target group attachment #####
resource "aws_alb_target_group_attachment" "alb_instance1" {
  target_group_arn = var.target_arn
  target_id = "${aws_instance.nginx.id}"
  port = 80
}



###### Private Instance ########


# Wordpress  ec2 instance
resource "aws_instance" "wordpress"{
  ami = var.instance_ami
  instance_type = var.instance_type
  key_name = var.key_name
  vpc_security_group_ids = var.security_groups
  subnet_id = var.subnet2_id
  tags = {
      Name = "Wordpress-host"
  }
}

#### AMI Creation for  Wordpress#####

resource "aws_ami_from_instance" "ami-wp" {
  name               = "ami-wp"
  source_instance_id = aws_instance.wordpress.id
}
resource "aws_alb_target_group_attachment" "alb_instance2" {
  target_group_arn = var.target_arn_wp
  target_id = "${aws_instance.wordpress.id}"
  port = 80
}
## Launch configuration for Private Instance ###

resource "aws_launch_configuration" "wp-lc" {
  name_prefix   = "wp-lc"
  image_id      = aws_ami_from_instance.ami-wp.id # Amazon Linux 2 AMI (HVM), SSD Volume Type
  instance_type = var.instance_type
  key_name      = var.key_name
  security_groups             = var.security_groups
  associate_public_ip_address = true
  lifecycle {
    create_before_destroy = true
  }


}

## Auto Scaling for Private Instance ###

resource "aws_autoscaling_group" "asg-wp" {
  name = "${aws_launch_configuration.wp-lc.name}-asg"


  min_size         = var.min_size
  desired_capacity = var.desired_capacity
  max_size         = var.max_size



  launch_configuration = aws_launch_configuration.wp-lc.name


  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]


  metrics_granularity = "1Minute"


  vpc_zone_identifier = [
    var.subnet2_id,
    var.subnet4_id
  ]


  # Required to redeploy without an outage.
  lifecycle {
    create_before_destroy = true
  }

  tag {

    key                 = "Name"
    value               = "web"
    propagate_at_launch = true
  }
}




###Control Node for Ansible Operations####

#control-node  ec2 instance
resource "aws_instance" "cn"{
  ami = var.instance_ami
  instance_type = var.instance_type
  key_name = var.key_name
  vpc_security_group_ids = var.security_groups
  subnet_id = var.subnet1_id
  tags = {
      Name = "TF-Control-Node"
  }
}




