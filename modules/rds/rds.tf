##### RDS Security Group  ######
resource "aws_security_group" "rds-sg" {
  name = "Demo-rds-sg"
  description = "RDS security group"
  vpc_id = var.vpc_id
   ingress {
    description = "MYSQL/Aurora"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["103.162.196.218/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "RDS-Security-Group"
  }
}



##### Database subnet Group  ######

resource "aws_db_subnet_group" "demo-db-subnet" {
  name       = "db-subnet-demo"
  description = " Subnet group for RDS instance creation"
  subnet_ids = ["${var.subnet1_id}","${var.subnet2_id}","${var.subnet3_id}","${var.subnet4_id}"]
  tags = {
    Name = "My DB subnet group"
  }
}
#### RDS Instance Creation ####

resource "aws_db_instance" "demo-db" {
  allocated_storage        = 256 # gigabytes
  backup_retention_period  = 7   # in days
  db_subnet_group_name     = aws_db_subnet_group.demo-db-subnet.id
  engine                   = "MySQL"
  engine_version           = "8.0.20"
  identifier               = "demo-db"
  instance_class           = "db.t3.large"
  multi_az                 = false
  name                     = "demo_db"
  parameter_group_name     = "default.mysql8.0" # if you have tuned it
  password                 = var.db_password
  port                     = 3306
  publicly_accessible      = false
  storage_encrypted        = true # you should always do this
  storage_type             = "gp2"
  username                 = var.db_username
  vpc_security_group_ids   = ["${aws_security_group.rds-sg.id}"]
  final_snapshot_identifier= true
}


