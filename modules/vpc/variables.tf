variable "vpc_cidr" {
    default= "10.0.0.0/16"
}

variable "tenancy" {
    default= "dedicated"
}


variable "demo-subnet-public-1_cidr" {
    default= "10.0.1.0/24"
}
variable "demo-subnet-public-2_cidr" {
    default= "10.0.2.0/24"
}
variable "table_cidr" {
    default= "0.0.0.0/0"
}


variable "demo-subnet-private-1_cidr" {
    default= "10.0.3.0/24"
}
variable "demo-subnet-private-2_cidr" {
    default= "10.0.4.0/24"
}

