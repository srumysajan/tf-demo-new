# VPC Creation

resource "aws_vpc" "demo-vpc" {
    cidr_block       = var.vpc_cidr
    instance_tenancy = var.tenancy
    tags = {
        Name = "Demo-vpc"
    }
}

# Public Subnet -1  Creation

resource "aws_subnet" "demo-subnet-public-1" {
    vpc_id = aws_vpc.demo-vpc.id
    cidr_block =  var.demo-subnet-public-1_cidr
    map_public_ip_on_launch = "true" //it makes this a public subnet
    availability_zone = var.az[0]
    tags = {
        Name = "Demo-subnet-public-1"
    }
}

# Public Subnet -2  Creation

resource "aws_subnet" "demo-subnet-public-2" {
    vpc_id = aws_vpc.demo-vpc.id
    cidr_block =  var.demo-subnet-public-2_cidr
    map_public_ip_on_launch = "true" //it makes this a public subnet
    availability_zone = var.az[1]
    tags = {
        Name = "Demo-subnet-public-2"
    }
}

# Internet Gateway

resource "aws_internet_gateway" "demo-igw" {
    vpc_id = aws_vpc.demo-vpc.id
    tags = {
        Name = "Demo-igw"
    }
}

# Route table

resource "aws_route_table" "demo-public-crt" {
    vpc_id = aws_vpc.demo-vpc.id

    route {
        //associated subnet can reach everywhere
        cidr_block = var.table_cidr
        //CRT uses this IGW to reach internet
        gateway_id = aws_internet_gateway.demo-igw.id
    }

    tags = {
        Name = "Demo-public-crt"
    }
}

# Route Table Entry and Subnet Association

resource "aws_route_table_association" "demo-crta-public-subnet-1" {
    subnet_id = aws_subnet.demo-subnet-public-1.id
    route_table_id = aws_route_table.demo-public-crt.id
}


# Route Table Entry and Subnet Association

resource "aws_route_table_association" "demo-crta-public-subnet-2" {
    subnet_id = aws_subnet.demo-subnet-public-2.id
    route_table_id = aws_route_table.demo-public-crt.id
}


# Private Subnet-1

resource "aws_subnet" "demo-subnet-private-1" {
    vpc_id = aws_vpc.demo-vpc.id
    cidr_block =  "${var.demo-subnet-private-1_cidr}"
    availability_zone = var.az[1]
    tags = {
        Name = "Demo-subnet-private-1"
    }
}

# Private Subnet-2

resource "aws_subnet" "demo-subnet-private-2" {
    vpc_id = aws_vpc.demo-vpc.id
    cidr_block =  "${var.demo-subnet-private-2_cidr}"
    availability_zone = var.az[2]
    tags = {
        Name = "Demo-subnet-private-2"
    }
}
variable "az" {
  type    = list(any)
  default = ["ap-southeast-1a","ap-southeast-1b", "ap-southeast-1c"]
}



# Elastic ip
resource "aws_eip" "elastic_ip" {
  vpc      = true
}

# NAT gateway
resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.elastic_ip.id
  subnet_id     = aws_subnet.demo-subnet-public-1.id

  tags = {
    Name = "nat-gateway"
  }
}

# Route table with target as NAT gateway
resource "aws_route_table" "NAT_route_table" {
   vpc_id = aws_vpc.demo-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_gateway.id
  }

  tags = {
    Name = "NAT-route-table"
  }
}

# Associate route table to private subnet-1
resource "aws_route_table_association" "associate_routetable_to_private_subnet-1" {

  subnet_id      = aws_subnet.demo-subnet-private-1.id
  route_table_id = aws_route_table.NAT_route_table.id
}

# Associate route table to private subnet-2
resource "aws_route_table_association" "associate_routetable_to_private_subnet-2" {

  subnet_id      = aws_subnet.demo-subnet-private-2.id
  route_table_id = aws_route_table.NAT_route_table.id
}

##### ALB Security Group ######
resource "aws_security_group" "lb-sg" {
  name = "Demo-lb-sg"
  description = "Load balancer security group"
  vpc_id = aws_vpc.demo-vpc.id

   ingress {
    description = "allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["103.162.196.218/32"]
  }

  ingress {
    description = "allow HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["165.225.124.115/32"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
 tags = {
    Name = "LB-Security-Group"
  }
}
######## ALB ############
resource "aws_lb" "demo-lb" {
    name = "demo-lb"
    internal = false
    load_balancer_type = "application"
    security_groups = ["${aws_security_group.lb-sg.id}"]
    subnets = ["${aws_subnet.demo-subnet-public-1.id}","${aws_subnet.demo-subnet-public-2.id}"]
    tags = {
    Name = "Demo-LB"
  }
}
##### ALB Target Group
resource "aws_alb_target_group" "lb-tg" {
  health_check {
        interval=10
        path="/"
        protocol = "HTTP"
        timeout=5
        healthy_threshold =5
        unhealthy_threshold =2
  }
  name = "demo-lb-tg"
  port = 80
  protocol = "HTTP"
  vpc_id = aws_vpc.demo-vpc.id
}
###### LB Listner #####
resource "aws_alb_listener" "lb-listner" {
  load_balancer_arn = "${aws_lb.demo-lb.arn}"
  port = "80"
  protocol = "HTTP"
  default_action {
      target_group_arn = "${aws_alb_target_group.lb-tg.arn}"
      type = "forward"
  }
}
######## ALB Wordpress ############
resource "aws_lb" "demo-lb-wp" {
    name = "demo-lb-wp"
    internal = false
    load_balancer_type = "application"
    security_groups = ["${aws_security_group.lb-sg.id}"]
    subnets = ["${aws_subnet.demo-subnet-private-1.id}","${aws_subnet.demo-subnet-private-2.id}"]
    tags = {
    Name = "Demo-LB-WP"
  }
}

##### ALB Target Group Wordpress ####
resource "aws_alb_target_group" "lb-tg-wp" {
  health_check {
        interval=10
        path="/"
        protocol = "HTTP"
        timeout=5
        healthy_threshold =5
        unhealthy_threshold =2
  }
  name = "demo-lb-tg-wp"
  port = 80
  protocol = "HTTP"
  vpc_id = aws_vpc.demo-vpc.id
}

###### LB Listner Wordpress #####
resource "aws_alb_listener" "lb-listner-wp" {
  load_balancer_arn = "${aws_lb.demo-lb-wp.arn}"
  port = "80"
  protocol = "HTTP"
  default_action {
      target_group_arn = "${aws_alb_target_group.lb-tg-wp.arn}"
      type = "forward"
  }
}

##### EC2 Security Group  ######
resource "aws_security_group" "ec2-sg" {
  name = "Demo-ec2-sg"
  description = "EC2 security group"
  vpc_id = aws_vpc.demo-vpc.id

   ingress {
    description = "allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["103.162.196.218/32"]
  }

  ingress {
    description = "allow HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["165.225.124.115/32"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "EC2-Security-Group"
  }
}



