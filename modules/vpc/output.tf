output "vpc_id" {
        value =  aws_vpc.demo-vpc.id
}

output "subnet1_id" {
        value =  aws_subnet.demo-subnet-public-2.id
}
output "subnet2_id" {
        value =  aws_subnet.demo-subnet-private-2.id
}
output "subnet3_id" {
        value =  aws_subnet.demo-subnet-public-1.id
}
output "subnet4_id" {
        value =  aws_subnet.demo-subnet-private-1.id
}
output "security_groups" {
	value = ["${aws_security_group.ec2-sg.id}"]

}
output "target_arn" {
	 value ="${aws_alb_target_group.lb-tg.arn}"
}

output "target_arn_wp" {
	value ="${aws_alb_target_group.lb-tg-wp.arn}"
}




