
module "my_vpc" {
        source      = "/root/tf-demo/modules/vpc"
}
module "my_ec2"{
        source          	= "/root/tf-demo/modules/ec2"
        vpc_id          	= module.my_vpc.vpc_id
        subnet1_id      	= module.my_vpc.subnet1_id
        subnet2_id      	= module.my_vpc.subnet2_id
	subnet3_id              = module.my_vpc.subnet3_id
        subnet4_id              = module.my_vpc.subnet4_id
	security_groups 	= module.my_vpc.security_groups
	target_arn		= module.my_vpc.target_arn
	target_arn_wp		= module.my_vpc.target_arn_wp

}

module "my_rds"{
        source                  = "/root/tf-demo/modules/rds"
        vpc_id                  = module.my_vpc.vpc_id
        subnet1_id              = module.my_vpc.subnet1_id
        subnet2_id              = module.my_vpc.subnet2_id
        subnet3_id              = module.my_vpc.subnet3_id
        subnet4_id              = module.my_vpc.subnet4_id


}

